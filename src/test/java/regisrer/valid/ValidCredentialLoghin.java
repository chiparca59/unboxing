package regisrer.valid;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import util.BaseTestClass;

public class ValidCredentialLoghin extends BaseTestClass {

    //Test data

    protected static String validEmail = "radu@gmail.com";
    protected static String validPassword = "Pr375219.ra";

    @Before

    public void startPageBooking() {

        driver.get("http://138.68.69.185:3333");
    }
    @Test

    public void bookingStartPage() {

        WebElement button = driver.findElement(By.xpath("//button[text()='Login and start booking!']"));

        String actualMesage = button.getText();
        String expectedMasage = "Login and start booking!";

        Assert.assertEquals(actualMesage, expectedMasage);
    }

    @Test

    public void verifyLoghinWorksAsExpectedWhithValidCredential() {

        //click loghin
        driver.findElement(By.xpath("//button[text()='Login and start booking!']")).click();

        //click email field

        driver.findElement(By.xpath("//input[@placeholder ='Enter your email']")).click();

        //write valid address email

        driver.findElement(By.xpath("//input[@placeholder='Enter your email']")).sendKeys(validEmail);

        //click password field

        driver.findElement(By.xpath("//input[@type='password']")).click();

        //write valid password

        driver.findElement(By.xpath("//input[@placeholder='Enter your password']")).sendKeys(validPassword);

        // click loghin button

        driver.findElement(By.xpath("//button[@type='submit']")).click();

        //validate loghin successful

        Assert.assertEquals("Error mesage", "My account",
                driver.findElement(By.xpath(
                                "//div[@class='hover-icon text-sm bg-emerald-500 p-2 rounded font-bold']"))
                        .getText());

    }

}
