package regisrer.valid;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;

public class ReserveRoomPozitiv extends BaseTestClass {

    //Test data

    protected static String validEmail = "radu@gmail.com";
    protected static String validPassword = "Pr375219.ra";
    protected static String numberOfPerson = "2";
    protected static String numberOfRoom = "2";
    protected static String ckeckInDate = "18.11.2023";
    protected static String ckeckOutDate = "19.11.2023";




    @Before

    public void startPageBooking() {

        driver.get("http://138.68.69.185:3333");
    }
    @Test

    public void verifyIfICanReserveARoomInValidPeriod() {

        //click loghin
        driver.findElement(By.xpath("//button[text()='Login and start booking!']")).click();

        //click email field

        driver.findElement(By.xpath("//input[@placeholder ='Enter your email']")).click();

        //write valid address email

        driver.findElement(By.xpath("//input[@placeholder='Enter your email']")).sendKeys(validEmail);

        //click password field

        driver.findElement(By.xpath("//input[@type='password']")).click();

        //write valid password

        driver.findElement(By.xpath("//input[@placeholder='Enter your password']")).sendKeys(validPassword);

        // click loghin button

        driver.findElement(By.xpath("//button[@type='submit']")).click();

        // selectare hotel Iasi

        driver.findElement(By.xpath("//div[contains(text(),'Hotel Iasi')]")).click();

        // selectare camera hotel

        driver.findElement(By.xpath
                        ("//div/img[@src='https://www.parliament-hotel.ro/wp-content/uploads/2019/10/poza4-galerie-acasa.jpg']"))
                .click();
        //selectare numar persoane

        driver.findElement(By.xpath("//input[@type='number']")).sendKeys(numberOfPerson);

        // click 'Next Step' button

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        // selectare numar camera dorita

        driver.findElement(By.xpath("//input[@type='text']")).sendKeys(numberOfRoom);

        // selectare perioada de inceput rezervare

        driver.findElement(By.xpath("//input[@type='date']")).sendKeys(ckeckInDate);

        // selectare perioada de sfarsit rezervare

        driver.findElement(By.xpath("//input[@value='']"))
                .sendKeys(ckeckOutDate);

        // click button Next step

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        // optiuni mic dejun/animale de companie

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        //optiuni de plata

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        // click buton confirmare rezervare

//        driver.findElement(By.xpath("//button[contains(text(),'Book holiday')]")).click();
//
//        // click buton confirmare rezervare
//
//        driver.findElement(By.xpath("//button[contains(text(),'Book holiday')]")).click();
//
//        // verificare eroare rezervare
//
//        Assert.assertEquals("This property has no availability on our site","Succes!"
//                ,driver.findElement(By.xpath
//                        ("//div[contains(text(),'Succes!')]")).getText());
    }

}
