package regisrer.invalid;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;


import java.time.Duration;

public class ReserveRoomNegative extends BaseTestClass {

    //Test data

    protected static String validEmail = "radu@gmail.com";
    protected static String validPassword = "Pr375219.ra";
    protected static String country ="Romania";



    @Before

    public void startPageBooking() {

        driver.get("http://138.68.69.185:3333");
    }
    @Test

    public void verifyIfICanReserveARoomInInvalidPeriod() {

        //click loghin
        driver.findElement(By.xpath("//button[text()='Login and start booking!']")).click();

        //click email field

        driver.findElement(By.xpath("//input[@placeholder ='Enter your email']")).click();

        //write valid address email

        driver.findElement(By.xpath("//input[@placeholder='Enter your email']")).sendKeys(validEmail);

        //click password field

        driver.findElement(By.xpath("//input[@type='password']")).click();

        //write valid password

        driver.findElement(By.xpath("//input[@placeholder='Enter your password']")).sendKeys(validPassword);

        // click loghin button

        driver.findElement(By.xpath("//button[@type='submit']")).click();

        // setare timp asteptare incarcare pagina

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        //validate Update My account

        Assert.assertEquals("Error mesage", "My account",
                driver.findElement(By.xpath(
                                "//div[@class='hover-icon text-sm bg-emerald-500 p-2 rounded font-bold']"))
                        .getText());
        // selectare hotel Iasi

        driver.findElement(By.xpath("//div[contains(text(),'Hotel Iasi')]")).click();

        // selectare camera hotel

        driver.findElement(By.xpath
                ("//div/img[@src='https://www.parliament-hotel.ro/wp-content/uploads/2019/10/poza4-galerie-acasa.jpg']"))
                .click();
        //selectare numar persoane

        driver.findElement(By.xpath("//input[@type='number']")).sendKeys("2");

        // click 'Next Step' button

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        // selectare numar camera dorita

        driver.findElement(By.xpath("//input[@type='text']")).sendKeys("2");

        // selectare perioada de inceput rezervare

        driver.findElement(By.xpath("//input[@type='date']")).sendKeys("16.10.2023");

        // selectare perioada de sfarsit rezervare

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.findElement(By.xpath("//input[@value='']"))
                .sendKeys("20.10.2023");

        // click button Next step

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        // optiuni mic dejun/animale de companie

        Assert.assertEquals("What is the number of people eating breakfast?",
                driver.findElement(By.xpath("//div[contains(text(),'What is the number of people eating breakfast?')]")).getText());

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        //optiuni de plata

        driver.findElement(By.xpath("//button[contains(text(),'Next step')]")).click();

        // selectare buton confirmare rezervare

        driver.findElement(By.xpath("//button[contains(text(),'Book holiday')]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Book holiday')]")).click();

        // verificare eroare rezervare

        Assert.assertEquals("Reservation had been succesfully","This property has no availability on our site"
                ,driver.findElement(By.xpath
                        ("//div[contains(text(),'This property has no availability on our site')]")).getText());
    }


}
