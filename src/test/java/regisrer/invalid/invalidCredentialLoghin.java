package regisrer.invalid;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;


public class invalidCredentialLoghin extends BaseTestClass {

    //Test data

    protected static String invalidEmail = "vali@gmail.com";
    protected static String invalidPassword = "Pr123456.ra";

    @Before

    public void startPageBooking() {

        driver.get("http://138.68.69.185:3333");
    }


    @Test

    public void invalidCredentialLoghin(){

        //click loghin

        driver.findElement(By.xpath("//button[text()='Login and start booking!']")).click();

        //click email field

        driver.findElement(By.xpath("//input[@placeholder ='Enter your email']")).click();

        //write valid address email

        driver.findElement(By.xpath("//input[@placeholder='Enter your email']"))
                .sendKeys(invalidEmail);

        //click password field

        driver.findElement(By.xpath("//input[@type='password']")).click();

        //write valid password

        driver.findElement(By.xpath("//input[@placeholder='Enter your password']"))
                .sendKeys(invalidPassword);

        // click loghin button

        driver.findElement(By.xpath("//button[@type='submit']")).click();

        //validate error mesage

        Assert.assertEquals("Welcome!","Request failed with status code 404",
                driver.findElement(By.xpath("//div[contains(text(),'404')]")).getText());


    }


}
