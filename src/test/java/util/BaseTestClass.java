package util;


import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class BaseTestClass {
    public WebDriver driver;

    @Before
    public void setUpTest() {
        //  driver for remote conection(permit computerului sa acceseze local din browser
        ////        un anumit site)
       // System.setProperty("webdriver.chrome.driver", DriversPath.getDriverDirPath() + "chromedriver" + DriversPath.getDriverExtension());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}