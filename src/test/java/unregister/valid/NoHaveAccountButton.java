package unregister.valid;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;

public class NoHaveAccountButton extends BaseTestClass {

    @Test

    public void verifyIfNoHaveAccountButtonIsAvaialble (){

        driver.get("http://138.68.69.185:3333");

        driver.findElement(By.xpath("//button[contains(text(),'Register here')]")).click();

        Assert.assertEquals("The button is disable","Create a new account!",
                driver.findElement(By.xpath("//div[contains(text(),'Create a new account!')]")).getText());



    }
}
