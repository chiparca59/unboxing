package unregister.valid;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.BaseTestClass;

import java.time.Duration;

public class MandatoryField extends BaseTestClass {

    // test data

    protected String email = "robert@gmail.com";

    @Test

    public void verifyIfpasswordFieldIsMandatory(){

        driver.get("http://138.68.69.185:3333/");

        driver.findElement(By.xpath("//div[contains(text(),'Login')]")).click();

        driver.findElement(By.xpath("//input[@placeholder='Enter your email']")).sendKeys(email);

        driver.findElement(By.xpath("//button[@type='submit']")).click();

        Assert.assertEquals("Login to start planning your trip!",
                driver.findElement(By.xpath("//div[contains(text(),'Login to start planning your trip!')]"))
                        .getText());



    }
}
