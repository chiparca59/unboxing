package unregister.valid;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;

public class Login_button extends BaseTestClass {

    //Test data

     protected String hotel = "Iasi";

    @Before

    public void startBookingPage(){

        driver.get("http://138.68.69.185:3333/");
    }

    @Test

    public void checkIfButtonLoginIsPresentOfTheEndOfThePage (){

        driver.findElement(By.xpath("//button[contains(text(),'Login and start booking!')]")).click();

        Assert.assertEquals("The button login is not function","Login to start planning your trip!",
                driver.findElement(By.xpath("//div[contains(text(),'Login to start planning your trip!')]"))
                        .getText());
    }

    @Test

    public void checkIfTheButtonLoginIsPresentOnTheSearchPage(){

      driver.findElement(By.xpath("//input[@placeholder='Where are you going?']")).sendKeys(hotel);

       Assert.assertTrue(driver.findElement(By.xpath("//div[contains(text(),'Login')]")).isEnabled());


    }
}
