package unregister.invalid;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;

public class ForgotPasswordButton extends BaseTestClass {

    @Test

    public void verifyIfForgotPasswordButtonIsAvailable(){

        driver.get("http://138.68.69.185:3333");

        driver.findElement(By.xpath("//div[contains(text(),'Login')]")).click();

        driver.findElement(By.xpath("//div[contains(text(),'Forgot your password?')]")).click();

        Assert.assertEquals("Redirected from reister page","Forgot your password?",
                driver.findElement(By.xpath("//div[contains(text(),'Forgot your password?')]"))
                        .getText());

    }
}
