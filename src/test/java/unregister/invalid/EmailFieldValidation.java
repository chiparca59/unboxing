package unregister.invalid;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;

public class EmailFieldValidation extends BaseTestClass {

    protected String emailField = "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeee";

    @Test

    public void verifyIfEmailFieldAccept50Charater() {

        driver.get("http://138.68.69.185:3333");

        driver.findElement(By.xpath("//div[contains(text(),'Login')]")).click();

        driver.findElement(By.xpath("//input[@type='text']")).sendKeys(emailField);

        String completedField = driver.findElement(By.xpath("//input[@type='text']")).getText();

        boolean theFieldHave50Character = true;

        if (completedField.equals(emailField)) {
            theFieldHave50Character = true;
        } else {
            theFieldHave50Character = false;
        }

        Assert.assertFalse("The field Have 50 characters",theFieldHave50Character);

    }
}
