package unregister.invalid;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import util.BaseTestClass;

public class Button_sTextNoHaveAcces extends BaseTestClass {

    @Test

    public void verifyTheButtonSNameRedirectToRegisterPageIsProperly(){

        driver.get("http://138.68.69.185:3333");

        Assert.assertNotEquals("Button's name is properly with requireement",
                "'Don't have an account yet? Created one!'",
    driver.findElement(By.xpath("//button[contains(text(),'Register here')]")).getText());
    }
}
